<?php
/**
 * This file has been @generated by a phing task by {@link GeneratePhonePrefixData}.
 * See [README.md](README.md#generating-data) for more information.
 *
 * Pull requests changing data in these files will not be accepted. See the
 * [FAQ in the README](README.md#problems-with-invalid-numbers] on how to make
 * metadata changes.
 *
 * Do not modify this file directly!
 */

return array (
  50230 => 'Crdb',
  50231 => 'Crdb',
  50232 => 'Crdb',
  5023229 => 'Telgua',
  50233 => 'Crdb',
  50234 => 'Movistar',
  502350 => 'Movistar',
  502351 => 'Movistar',
  502352 => 'Movistar',
  502353 => 'Movistar',
  502354 => 'Movistar',
  502355 => 'Movistar',
  502356 => 'Movistar',
  502370 => 'Crdb',
  502371 => 'Crdb',
  502372 => 'Crdb',
  502373 => 'Crdb',
  502374 => 'Crdb',
  502375 => 'Crdb',
  50239 => 'Telgua',
  502390 => 'Crdb',
  502391 => 'Crdb',
  502392 => 'Crdb',
  502393 => 'Crdb',
  50240 => 'Crdb',
  502400 => 'Movistar',
  50241 => 'Telgua',
  50242 => 'Telgua',
  50243 => 'Movistar',
  50244 => 'Movistar',
  5024476 => 'Crdb',
  5024477 => 'Crdb',
  5024478 => 'Crdb',
  5024479 => 'Crdb',
  502448 => 'Crdb',
  502449 => 'Crdb',
  50245 => 'Crdb',
  50246 => 'Crdb',
  50247 => 'Telgua',
  502477 => 'Crdb',
  502478 => 'Crdb',
  502479 => 'Crdb',
  50248 => 'Crdb',
  50249 => 'Crdb',
  502500 => 'Crdb',
  502501 => 'Telgua',
  502502 => 'Movistar',
  502503 => 'Crdb',
  502504 => 'Crdb',
  502505 => 'Crdb',
  502506 => 'Crdb',
  502507 => 'Movistar',
  502508 => 'Movistar',
  502509 => 'Movistar',
  502510 => 'Movistar',
  502511 => 'Telgua',
  502512 => 'Telgua',
  502513 => 'Telgua',
  502514 => 'Movistar',
  502515 => 'Crdb',
  502516 => 'Crdb',
  502517 => 'Crdb',
  502518 => 'Crdb',
  502519 => 'Crdb',
  50252 => 'Movistar',
  502520 => 'Crdb',
  50253 => 'Crdb',
  5025310 => 'Telgua',
  5025311 => 'Telgua',
  5025312 => 'Movistar',
  5025313 => 'Movistar',
  502539 => 'Movistar',
  50254 => 'Telgua',
  502540 => 'Movistar',
  502550 => 'Movistar',
  502551 => 'Telgua',
  5025518 => 'Movistar',
  5025519 => 'Movistar',
  502552 => 'Crdb',
  5025531 => 'Telgua',
  5025532 => 'Telgua',
  5025533 => 'Telgua',
  5025534 => 'Telgua',
  5025535 => 'Telgua',
  5025536 => 'Telgua',
  5025537 => 'Telgua',
  5025538 => 'Telgua',
  5025539 => 'Telgua',
  502554 => 'Movistar',
  5025543 => 'Telgua',
  5025544 => 'Telgua',
  502555 => 'Telgua',
  5025550 => 'Crdb',
  5025551 => 'Crdb',
  5025552 => 'Crdb',
  5025553 => 'Crdb',
  502556 => 'Telgua',
  502557 => 'Telgua',
  502558 => 'Telgua',
  5025580 => 'Crdb',
  5025581 => 'Crdb',
  502559 => 'Telgua',
  50256 => 'Movistar',
  502561 => 'Telgua',
  502562 => 'Telgua',
  502563 => 'Telgua',
  502569 => 'Telgua',
  50257 => 'Crdb',
  502571 => 'Telgua',
  502579 => 'Movistar',
  50258 => 'Telgua',
  502580 => 'Crdb',
  5025819 => 'Crdb',
  502588 => 'Crdb',
  502589 => 'Crdb',
  50259 => 'Telgua',
  502590 => 'Crdb',
  5025915 => 'Movistar',
  5025916 => 'Movistar',
  5025917 => 'Movistar',
  5025918 => 'Crdb',
  5025919 => 'Crdb',
  502599 => 'Crdb',
  5028 => 'Crdb',
);
