<?php

namespace Lockminds\NBCPaymentGateway\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class NBCApiSetting extends MasterModel
{
    use SoftDeletes;
}
